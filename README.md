# Proyecto PHP

Este es un proyecto de ejemplo desarrollado con el objetivo de practicar con el despliegue de aplicaciones desarrolladas en el lenguaje PHP a través de la herramienta Jenkins y el enlace con Gitlab.

## Versión

1.0.0 - Marzo 2021

## Autores

* **Jorge Alfonso Solís**